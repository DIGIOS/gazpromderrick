const uint8_t RELAY_PIN = 3;

void setup() {
    Serial.begin(115200);
    pinMode(RELAY_PIN, OUTPUT);
}

void loop() {

}

void serialEvent() {
    String command = Serial.readStringUntil('\n');

    if (command == "on")
    {
        digitalWrite(RELAY_PIN, HIGH);
    }
    else if (command == "off")
    {
        digitalWrite(RELAY_PIN, LOW);
    }
    else
    {
        Serial.println(F("BAD COMMAND"));
    }
}
