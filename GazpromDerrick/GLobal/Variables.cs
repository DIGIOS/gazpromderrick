﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GazpromDerrick
{
    public class Variables
    {
        private static string dataDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data\\");
        public static string DataDir
        {
            get
            {
                // Если директории не существует, то она будет создана
                if (!Directory.Exists(dataDir)) Directory.CreateDirectory(dataDir);
                return dataDir;
            }
        }

        private static string logsDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "logs\\");
        public static string LogsDir
        {
            get
            {
                // Если директории не существует, то она будет создана
                if (!Directory.Exists(logsDir)) Directory.CreateDirectory(logsDir);
                return logsDir;
            }
        }

        private static string configFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "config.ini");
        public static string ConfigFile
        {
            get
            {
                return configFile;
            }
        }
    }
}
