﻿using GazpromDerrick.TimeLine;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using WpfScreenHelper;
using LibVLCSharp.Shared;

namespace GazpromDerrick
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private SerialPort arduino;

        private ActionTimeline timeLine;
        private BottomWindow bottomWindow;
        private DispatcherTimer firstInitTimer;

        public static RoutedCommand CloseCommand = new RoutedCommand();

        private LibVLC libVLC;
        private LibVLCSharp.Shared.MediaPlayer mediaPlayer;

        public MainWindow()
        {
            InitializeComponent();

            ControlsInit();

            CloseCommand.InputGestures.Add(new KeyGesture(Key.Q, ModifierKeys.Control));

            arduino = new SerialPort(Properties.Settings.Default.ComPort, 115200);

            InitMediaPlayer();

            timeLine = new ActionTimeline(mediaPlayer);
            //timeLine.CurrentStateChanged += TimelineStateChanged;

            bottomWindow = new BottomWindow();

            layoutBlank.HiddenCompleted += (s, e) =>
            {
                SendCommand("off");
                timeLine.ShowMenu();
            };

            layoutBlank.ShowedCompleted += (s, e) =>
            {
                SendCommand("on");
            };

            layoutBlank.ShowedDemo += (s, e) =>
            {
                demoControl.BeginFadeIn();
            };

            demoControl.HiddenCompleted += (s, e) =>
            {
                SendCommand("on");
                bottomWindow.Stop();
            };

            demoControl.ShowedCompleted += (s, e) =>
            {
                SendCommand("off");
                bottomWindow.DemoPlay();
            };

            this.IsEnabled = false;

            firstInitTimer = new DispatcherTimer();
            firstInitTimer.Tick += new EventHandler(firstInitTimer_Tick);
            //firstInitTimer.Interval = new TimeSpan(0, 0, 10);
            firstInitTimer.Interval = new TimeSpan(0, 0, 1);
        }

        private void InitMediaPlayer()
        {
            libVLC = new LibVLC();
            mediaPlayer = new LibVLCSharp.Shared.MediaPlayer(libVLC);

            videoView.Loaded += (sender, e) => videoView.MediaPlayer = mediaPlayer;

            //mediaPlayer.Media = new Media(libVLC, Variables.DataDir + "verh_full.mp4", Media.FromType.FromPath);
        }

        private void ControlsInit()
        {
            //mainMenu.MenuItemClick += mainMenu_MenuItem1Click;
            //mainMenu.ToLayoutClick += mainMenu_ToLayoutClick;
            //mainMenu.DrillingClick += MainMenu_DrillingClick;

            drillingControl.ButtonClick += DrillingControl_ButtonClick;

            backControl.CloseClick += backControl_CloseClick;
        }

        private void firstInitTimer_Tick(object sender, EventArgs e)
        {
            firstInitTimer.Stop();
            SendCommand("on");
            this.IsEnabled = true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            OpenSecondWindows();
            
            //bottomWindow.SWellPlay();
            //timeLine.HideMenu();

            try
            {
                arduino.Open();
                SendCommand("on");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            firstInitTimer.Start();
        }

        private void SendCommand(string data)
        {
            try
            {
                if (arduino.IsOpen)
                    arduino.Write(data + "\n");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        private void OpenSecondWindows()
        {
            List<Screen> screenList = Screen.AllScreens.ToList<Screen>();

            Screen leftScreen = screenList.Find(x => x.DeviceName.EndsWith(Properties.Settings.Default.DisplayLeft));
            if (leftScreen != null)
            {
                bottomWindow.Left = leftScreen.Bounds.Left;
                bottomWindow.Top = leftScreen.Bounds.Top;
                bottomWindow.Width = leftScreen.Bounds.Width * 3;
                bottomWindow.Height = leftScreen.Bounds.Height;

                bottomWindow.Show();
            }
                
            bottomWindow.Show();
        
            /*Screen leftScreen = screenList[1];
            if (leftScreen != null)
            {
                bottomWindow.Left = leftScreen.Bounds.Left;
                bottomWindow.Top = leftScreen.Bounds.Top;
                bottomWindow.Width = leftScreen.Bounds.Width * 3;
                bottomWindow.Height = leftScreen.Bounds.Height;

                bottomWindow.Show();
            }*/
        }

        private void mainMenu_MenuItem1Click(object sender, RoutedEventArgs e)
        {
            Button bt = (sender as Button);

            logger.Info("Menu " + bt.Name + " click");

            if (bt.Name == "menuItem1")
            {
                timeLine.ShowMenu1();
            }
            else if (bt.Name == "menuItem2")
            {
                timeLine.ShowMenu2();
            }
            else if (bt.Name == "menuItem3")
            {
                timeLine.ShowMenu3();
            }
            else if (bt.Name == "menuItem4")
            {
                timeLine.ShowMenu4();
            }
            else if (bt.Name == "menuItem5")
            {
                timeLine.ShowMenu5();
            }
            else if (bt.Name == "menuItem6")
            {
                timeLine.ShowMenu6();
            }
        }

        private void TimelineStateChanged(object sender, EventArgs e)
        {
            mainMenu.Visibility = Visibility.Hidden;
            backControl.Visibility = Visibility.Hidden;
            drillingControl.Visibility = Visibility.Hidden;

            switch (timeLine.CurrentState)
            {
                case ActionTimeline.TimeLineState.MenuLoop:
                    mainMenu.Visibility = Visibility.Visible;
                    break;
                case ActionTimeline.TimeLineState.MenuItem1Loop:
                    backControl.SetPosition("MenuItem1");
                    backControl.Visibility = Visibility.Visible;
                    break;
                case ActionTimeline.TimeLineState.MenuItem2Loop:
                    backControl.SetPosition("MenuItem2");
                    backControl.Visibility = Visibility.Visible;
                    break;
                case ActionTimeline.TimeLineState.MenuItem3Loop:
                    backControl.SetPosition("MenuItem3");
                    backControl.Visibility = Visibility.Visible;
                    break;
                case ActionTimeline.TimeLineState.MenuItem4Loop:
                    backControl.SetPosition("MenuItem4");
                    backControl.Visibility = Visibility.Visible;
                    break;
                case ActionTimeline.TimeLineState.MenuItem5Loop:
                    backControl.SetPosition("MenuItem5");
                    backControl.Visibility = Visibility.Visible;
                    break;
                case ActionTimeline.TimeLineState.MenuItem6Loop:
                    backControl.SetPosition("MenuItem6");
                    backControl.Visibility = Visibility.Visible;
                    break;
                case ActionTimeline.TimeLineState.LoopDrilling:
                    drillingControl.Visibility = Visibility.Visible;
                    break;
                case ActionTimeline.TimeLineState.HideMenu:
                    layoutBlank.BeginFadeIn();
                    break;
                default:
                    break;
            }
        }

        private void backControl_CloseClick(object sender, RoutedEventArgs e)
        {
            logger.Info("Back click");

            switch (timeLine.CurrentState)
            {
                case ActionTimeline.TimeLineState.MenuItem1Loop:
                    timeLine.HideMenu1();
                    break;
                case ActionTimeline.TimeLineState.MenuItem2Loop:
                    timeLine.HideMenu2();
                    break;
                case ActionTimeline.TimeLineState.MenuItem3Loop:
                    timeLine.HideMenu3();
                    break;
                case ActionTimeline.TimeLineState.MenuItem4Loop:
                    timeLine.HideMenu4();
                    break;
                case ActionTimeline.TimeLineState.MenuItem5Loop:
                    timeLine.HideMenu5();
                    break;
                case ActionTimeline.TimeLineState.MenuItem6Loop:
                    timeLine.HideMenu6();
                    break;
                default:
                    break;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void mainMenu_ToLayoutClick(object sender, RoutedEventArgs e)
        {
            logger.Info("Back to layout");

            timeLine.HideMenu();
            //layoutBlank.BeginFadeIn();
        }

        private void MainMenu_DrillingClick(object sender, RoutedEventArgs e)
        {
            logger.Info("Menu Drilling click");

            timeLine.ShowDrilling();
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        private void DrillingControl_ButtonClick(object sender, RoutedEventArgs e)
        {
            Button bt = (sender as Button);

            logger.Info("Menu drilling " + bt.Name + " click");

            if (bt.Name == "sWellButton")
            {
                bottomWindow.SWellPlay();
            }
            else if (bt.Name == "verticalWellButton")
            {
                bottomWindow.VerticalWellPlay();
            }
            else if (bt.Name == "subWellButton")
            {
                bottomWindow.SubWellPlay();
            }
            else if (bt.Name == "backButton")
            {
                bottomWindow.Stop();
                timeLine.HideDrilling();
            }
        }

        private void MediaPlayer_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            //MessageBox.Show(e.ErrorException.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
