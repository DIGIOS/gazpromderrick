﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GazpromDerrick.Controls
{
    /// <summary>
    /// Логика взаимодействия для MainMenuControl.xaml
    /// </summary>
    public partial class MainMenuControl : UserControl
    {
        public event RoutedEventHandler MenuItemClick;

        public event RoutedEventHandler ToLayoutClick;

        public event RoutedEventHandler DrillingClick;


        public MainMenuControl()
        {
            InitializeComponent();
        }

        private void menuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItemClick?.Invoke(sender, e);
        }

        private void toLayout_Click(object sender, RoutedEventArgs e)
        {
            ToLayoutClick?.Invoke(sender, e);
        }

        private void DrillingButton_Click(object sender, RoutedEventArgs e)
        {
            DrillingClick?.Invoke(sender, e);
        }
    }
}
