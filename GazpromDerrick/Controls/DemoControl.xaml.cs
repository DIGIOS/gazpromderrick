﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GazpromDerrick.Controls
{
    /// <summary>
    /// Логика взаимодействия для DemoControl.xaml
    /// </summary>
    public partial class DemoControl : UserControl
    {
        public event RoutedEventHandler HiddenCompleted;
        public event RoutedEventHandler ShowedCompleted;

        public DemoControl()
        {
            InitializeComponent();

            mediaPlayer.Source = new Uri(Variables.DataDir + "verh_demo.mp4");
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            /*backButton.IsEnabled = false;
            BeginFadeOut();*/
        }

        public void BeginFadeOut()
        {
            DoubleAnimation opacityAnimation = new DoubleAnimation();
            opacityAnimation.From = this.Opacity;
            opacityAnimation.To = 0;
            opacityAnimation.Duration = TimeSpan.FromMilliseconds(300);
            opacityAnimation.Completed += (s, e) =>
            {
                this.Visibility = Visibility.Hidden;
                mediaPlayer.Stop();
                HiddenCompleted?.Invoke(this, null);
            };
            this.BeginAnimation(Button.OpacityProperty, opacityAnimation);
        }

        public void BeginFadeIn()
        {
            mediaPlayer.Play();

            this.Opacity = 0;
            this.Visibility = Visibility.Visible;

            DoubleAnimation opacityAnimation = new DoubleAnimation();
            opacityAnimation.From = this.Opacity;
            opacityAnimation.To = 1;
            opacityAnimation.Duration = TimeSpan.FromMilliseconds(300);
            opacityAnimation.Completed += (s, e) =>
            {
                ShowedCompleted?.Invoke(this, null);
            };
            this.BeginAnimation(Button.OpacityProperty, opacityAnimation);
        }

        private void PalyerLeftSecondWindow_MediaEnded(object sender, RoutedEventArgs e)
        {
            mediaPlayer.Position = TimeSpan.Zero;
            mediaPlayer.Play();
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            BeginFadeOut();
        }
    }
}
