﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GazpromDerrick.Controls
{
    /// <summary>
    /// Логика взаимодействия для DrillingControl.xaml
    /// </summary>
    public partial class DrillingControl : UserControl
    {
        public event RoutedEventHandler ButtonClick;

        public DrillingControl()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ButtonClick?.Invoke(sender, e);
        }
    }
}
