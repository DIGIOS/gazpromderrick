﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GazpromDerrick.Controls
{
    /// <summary>
    /// Логика взаимодействия для BackControl.xaml
    /// </summary>
    public partial class BackControl : UserControl
    {
        public event RoutedEventHandler CloseClick;

        private Dictionary<string, Thickness> position = new Dictionary<string, Thickness>();

        public BackControl()
        {
            InitializeComponent();

            position.Add("MenuItem1", new Thickness(0, 0, 25, 960));
            position.Add("MenuItem2", new Thickness(0, 0, 25, 960));
            position.Add("MenuItem3", new Thickness(0, 0, 25, 960));
            position.Add("MenuItem4", new Thickness(0, 0, 25, 960));
            position.Add("MenuItem5", new Thickness(0, 0, 193, 420));
            position.Add("MenuItem6", new Thickness(0, 0, 25, 960));
        }

        public void SetPosition(string menuItemName)
        {
            Thickness margin = position[menuItemName];

            if (margin == null)
                margin = new Thickness(0);

            closeButton.Margin = margin;
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            CloseClick?.Invoke(sender, e);
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            CloseClick?.Invoke(sender, e);
        }
    }
}
