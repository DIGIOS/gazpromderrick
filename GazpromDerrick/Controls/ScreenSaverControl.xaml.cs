﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GazpromDerrick.Controls
{
    /// <summary>
    /// Логика взаимодействия для ScreenSaverControl.xaml
    /// </summary>
    public partial class ScreenSaverControl : UserControl
    {
        public event RoutedEventHandler HiddenCompleted;
        public event RoutedEventHandler ShowedCompleted;
        public event RoutedEventHandler ShowedDemo;

        public ScreenSaverControl()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            interactiveButton.IsEnabled = false;
            demoButton.IsEnabled = false;
            BeginFadeOut();
        }

        public void BeginFadeOut()
        {
            DoubleAnimation opacityAnimation = new DoubleAnimation();
            opacityAnimation.From = this.Opacity;
            opacityAnimation.To = 0;
            opacityAnimation.Duration = TimeSpan.FromMilliseconds(300);
            opacityAnimation.Completed += (s, e) =>
            {
                this.Visibility = Visibility.Hidden;
                HiddenCompleted?.Invoke(this, null);
            };
            this.BeginAnimation(Button.OpacityProperty, opacityAnimation);
        }

        public void BeginFadeIn()
        {
            this.Opacity = 0;
            this.Visibility = Visibility.Visible;

            DoubleAnimation opacityAnimation = new DoubleAnimation();
            opacityAnimation.From = this.Opacity;
            opacityAnimation.To = 1;
            opacityAnimation.Duration = TimeSpan.FromMilliseconds(300);
            opacityAnimation.Completed += (s, e) =>
            {
                ShowedCompleted?.Invoke(this, null);
                interactiveButton.IsEnabled = true;
                demoButton.IsEnabled = true;
            };
            this.BeginAnimation(Button.OpacityProperty, opacityAnimation);
        }

        private void DemoButton_Click(object sender, RoutedEventArgs e)
        {
            ShowedDemo?.Invoke(this, null);
        }
    }
}
