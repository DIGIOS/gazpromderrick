﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Reflection;
using LibVLCSharp.Shared;

namespace GazpromDerrick
{
    /// <summary>
    /// Логика взаимодействия для BottomWindow.xaml
    /// </summary>
    public partial class BottomWindow : Window
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public static RoutedCommand CloseCommand = new RoutedCommand();

        private bool isLoop = false;
        private Uri CurrentVideoFile { get; set; }

        LibVLC libVLC;
        LibVLCSharp.Shared.MediaPlayer mediaPlayer;

        public BottomWindow()
        {
            InitializeComponent();

            CloseCommand.InputGestures.Add(new KeyGesture(Key.Q, ModifierKeys.Control));

            /*player.Source = new Uri(Variables.DataDir + "niz_sub-well.mp4");
            player.Play();*/

            /*var currentAssembly = Assembly.GetEntryAssembly();
            var currentDirectory = new FileInfo(currentAssembly.Location).DirectoryName;
            // Default installation path of VideoLAN.LibVLC.Windows
            var libDirectory = new DirectoryInfo(System.IO.Path.Combine(currentDirectory, "libvlc", IntPtr.Size == 4 ? "win-x86" : "win-x64"));

            vlcOptions = new string[]
            {
                //"--vout=any", "--avcodec-hw=dxva2", "--avcodec-threads=4", "--avcodec-skip-frame=-1"
                "--avcodec-hw=dxva2"
            };

            this.player.SourceProvider.CreatePlayer(libDirectory, vlcOptions);*/

            //this.player.SourceProvider.MediaPlayer.

            libVLC = new LibVLC();
            mediaPlayer = new LibVLCSharp.Shared.MediaPlayer(libVLC);
            mediaPlayer.EndReached += MediaPlayer_EndReached;

            videoView.Loaded += (sender, e) => videoView.MediaPlayer = mediaPlayer;

            SubWellPlay();

            //mediaPlayer.Media = new Media(libVLC, Variables.DataDir + "verh_full.mp4", Media.FromType.FromLocation);
        }

        private void MediaPlayer_EndReached(object sender, EventArgs e)
        {
            ImageFadeIn();
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        public void SubWellPlay()
        {
            //player.Source = new Uri(Variables.DataDir + "niz_sub-well.mp4");
            //player.SourceProvider.MediaPlayer.SetMedia(new Uri(Variables.DataDir + "niz_sub-well.mp4"), vlcOptions);
            //CurrentVideoFile = new Uri(Variables.DataDir + "niz_sub-well.mp4");
            mediaPlayer.Media = new Media(libVLC, Variables.DataDir + "niz_sub-well.mp4", Media.FromType.FromPath);
            isLoop = false;
            Play();
        }

        public void VerticalWellPlay()
        {
            //CurrentVideoFile = new Uri(Variables.DataDir + "niz_sub-well.mp4");
            //player.Source = new Uri(Variables.DataDir + "niz_vertikal-well.mp4");
            //player.SourceProvider.MediaPlayer.SetMedia(new Uri(Variables.DataDir + "niz_vertikal-well.mp4"), vlcOptions);
            mediaPlayer.Media = new Media(libVLC, Variables.DataDir + "niz_vertikal-well.mp4", Media.FromType.FromPath);
            isLoop = false;
            Play();
        }

        public void SWellPlay()
        {
            //CurrentVideoFile = new Uri(Variables.DataDir + "niz_s-well.wmv");
            //player.SourceProvider.MediaPlayer.SetMedia(new Uri(Variables.DataDir + "niz_s-well.mp4"));
            //player.Source = new Uri(Variables.DataDir + "niz_s-well.mp4");
            //player.SourceProvider.MediaPlayer.SetMedia(new Uri(Variables.DataDir + "niz_s-well.mp4"), vlcOptions);
            mediaPlayer.Media = new Media(libVLC, Variables.DataDir + "niz_s-well.mp4", Media.FromType.FromPath);
            isLoop = false;
            Play();
        }

        public void DemoPlay()
        {
            logger.Info("Bottom demo play");
            mediaPlayer.Media = new Media(libVLC, Variables.DataDir + "niz_demo.mp4", Media.FromType.FromPath);
            isLoop = true;
            Play();
        }

        public void Play()
        {
            //player.Position = TimeSpan.Zero;
            mediaPlayer.Time = 0;
            //if (pl)
            if (!mediaPlayer.IsPlaying)
                mediaPlayer.Play();
            //player.SourceProvider.MediaPlayer.Play(CurrentVideoFile);
            ImageFadeOut();
        }

        public void Stop()
        {
            ImageFadeIn();
        }

        private void ImageFadeOut()
        {
            DoubleAnimation opacityAnimation = new DoubleAnimation();
            opacityAnimation.From = image1.Opacity;
            opacityAnimation.To = 0;
            opacityAnimation.Duration = TimeSpan.FromMilliseconds(300);
            opacityAnimation.Completed += (s, e) =>
            {
                image1.Visibility = Visibility.Hidden;
            };
            image1.BeginAnimation(Button.OpacityProperty, opacityAnimation);
        }

        private void ImageFadeIn()
        {
            image1.Dispatcher.Invoke(new Action(() => {
                 image1.Opacity = 0;
                 image1.Visibility = Visibility.Visible;

                 DoubleAnimation opacityAnimation = new DoubleAnimation();
                 opacityAnimation.From = image1.Opacity;
                 opacityAnimation.To = 1;
                 opacityAnimation.Duration = TimeSpan.FromMilliseconds(300);
                 opacityAnimation.Completed += (s, e) =>
                 {
                     mediaPlayer.Stop();
                 };
                 image1.BeginAnimation(Button.OpacityProperty, opacityAnimation);
                 ;
            }));
        }

        private void PalyerLeftSecondWindow_MediaEnded(object sender, RoutedEventArgs e)
        {
            if (isLoop)
            {
                //player.Position = TimeSpan.Zero;
                mediaPlayer.Time = 0;
                if (!mediaPlayer.IsPlaying)
                    mediaPlayer.Play();
            }
            else
            {
                ImageFadeIn();
            }
        }

        private void Player_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            logger.Error(e);
        }
    }
}
