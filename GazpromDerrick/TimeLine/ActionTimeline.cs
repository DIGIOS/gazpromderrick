﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Threading;

namespace GazpromDerrick.TimeLine
{
    public class ActionTimeline
    {
        VideoItem showMenuItem;
        VideoItem menuLoopItem;
        VideoItem hideMenuItem;

        VideoItem showMenu1Item;
        VideoItem menu1LoopItem;
        VideoItem hideMenu1Item;

        VideoItem showMenu2Item;
        VideoItem menu2LoopItem;
        VideoItem hideMenu2Item;

        VideoItem showMenu3Item;
        VideoItem menu3LoopItem;
        VideoItem hideMenu3Item;

        VideoItem showMenu4Item;
        VideoItem menu4LoopItem;
        VideoItem hideMenu4Item;

        VideoItem showMenu5Item;
        VideoItem menu5LoopItem;
        VideoItem hideMenu5Item;

        VideoItem showMenu6Item;
        VideoItem menu6LoopItem;
        VideoItem hideMenu6Item;

        VideoItem showDrillingItem;
        VideoItem loopDrillingItem;
        VideoItem hideDrillingItem;

        DispatcherTimer timelineTimer = new DispatcherTimer();

        public event EventHandler CurrentStateChanged;

        public enum TimeLineState
        {
            Idle,

            ShowMenu,
            MenuLoop,
            BeginHideMenu,
            HideMenu,

            ShowMenuItem1,
            MenuItem1Loop,
            HideMenuItem1,

            ShowMenuItem2,
            MenuItem2Loop,
            HideMenuItem2,

            ShowMenuItem3,
            MenuItem3Loop,
            HideMenuItem3,

            ShowMenuItem4,
            MenuItem4Loop,
            HideMenuItem4,

            ShowMenuItem5,
            MenuItem5Loop,
            HideMenuItem5,

            ShowMenuItem6,
            MenuItem6Loop,
            HideMenuItem6,

            ShowDrilling,
            LoopDrilling,
            HideDrilling
        }

        public ActionTimeline(LibVLCSharp.Shared.MediaPlayer mediaElement)
        {
            #region Основное меню
            showMenuItem = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 0, 0, 1),
                timeEnd: new TimeSpan(0, 0, 0, 3, 800),
                isLoop: false);

            showMenuItem.MediaEnded += (o, e) =>
            {
                menuLoopItem.Play();
                CurrentState = TimeLineState.MenuLoop;
            };

            menuLoopItem = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 0, 3, 800),
                timeEnd: new TimeSpan(0, 0, 1, 6, 000),
                isLoop: true);

            hideMenuItem = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 1, 6, 000),
                timeEnd: new TimeSpan(0, 0, 1, 7, 700),
                isLoop: false);

            hideMenuItem.MediaEnded += (o, e) =>
            {
                mediaElement.Pause();
                CurrentState = TimeLineState.HideMenu;
            };

            #endregion

            #region 1 пункт меню
            showMenu1Item = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 1, 6, 100),
                timeEnd: new TimeSpan(0, 0, 1, 11, 720),
                isLoop: false);

            showMenu1Item.MediaEnded += (o, e) =>
            {
                menu1LoopItem.Play();
                CurrentState = TimeLineState.MenuItem1Loop;
            };

            menu1LoopItem = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 1, 11, 730),
                timeEnd: new TimeSpan(0, 0, 2, 4, 970),
                isLoop: true,
                isNewStart: false);

            hideMenu1Item = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 2, 4, 980),
                timeEnd: new TimeSpan(0, 0, 2, 11, 850),
                isLoop: false);

            hideMenu1Item.MediaEnded += (o, e) =>
            {
                //ShowMenu();
                menuLoopItem.Play();
                CurrentState = TimeLineState.MenuLoop;
            };
            #endregion

            #region 2 пункт меню
            showMenu2Item = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 3, 14, 260),
                timeEnd: new TimeSpan(0, 0, 3, 19, 890),
                isLoop: false);

            showMenu2Item.MediaEnded += (o, e) =>
            {
                menu2LoopItem.Play();
                CurrentState = TimeLineState.MenuItem2Loop;
            };

            menu2LoopItem = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 3, 19, 900),
                timeEnd: new TimeSpan(0, 0, 4, 13, 890),
                isLoop: true,
                isNewStart: false);

            hideMenu2Item = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 4, 13, 900),
                timeEnd: new TimeSpan(0, 0, 4, 19, 990),
                isLoop: false);

            hideMenu2Item.MediaEnded += (o, e) =>
            {
                menuLoopItem.Play();
                CurrentState = TimeLineState.MenuLoop;
            };
            #endregion

            #region 3 пункт меню
            showMenu3Item = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 5, 22, 100),
                timeEnd: new TimeSpan(0, 0, 5, 27, 490),
                isLoop: false);

            showMenu3Item.MediaEnded += (o, e) =>
            {
                menu3LoopItem.Play();
                CurrentState = TimeLineState.MenuItem3Loop;
            };

            menu3LoopItem = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 5, 27, 500),
                timeEnd: new TimeSpan(0, 0, 6, 23, 340),
                isLoop: true,
                isNewStart: false);

            hideMenu3Item = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 6, 23, 350),
                timeEnd: new TimeSpan(0, 0, 6, 30, 300),
                isLoop: false);

            hideMenu3Item.MediaEnded += (o, e) =>
            {
                menuLoopItem.Play();
                CurrentState = TimeLineState.MenuLoop;
            };
            #endregion

            #region 4 пункт меню
            showMenu4Item = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 7, 32, 500),
                timeEnd: new TimeSpan(0, 0, 7, 34, 300),
                isLoop: false);

            showMenu4Item.MediaEnded += (o, e) =>
            {
                menu4LoopItem.Play();
                CurrentState = TimeLineState.MenuItem4Loop;
            };

            menu4LoopItem = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 7, 34, 300),
                timeEnd: new TimeSpan(0, 0, 8, 34, 100),
                isLoop: true,
                isNewStart: false);

            hideMenu4Item = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 8, 34, 110),
                timeEnd: new TimeSpan(0, 0, 8, 40, 500),
                isLoop: false);

            hideMenu4Item.MediaEnded += (o, e) =>
            {
                menuLoopItem.Play();
                CurrentState = TimeLineState.MenuLoop;
            };
            #endregion

            #region 5 пункт меню
            showMenu5Item = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 9, 42, 600),
                timeEnd: new TimeSpan(0, 0, 9, 47, 240),
                isLoop: false);

            showMenu5Item.MediaEnded += (o, e) =>
            {
                menu5LoopItem.Play();
                CurrentState = TimeLineState.MenuItem5Loop;
            };

            menu5LoopItem = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 9, 47, 240),
                timeEnd: new TimeSpan(0, 0, 10, 42, 930),
                isLoop: true,
                isNewStart: false);

            hideMenu5Item = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 10, 42, 940),
                timeEnd: new TimeSpan(0, 0, 10, 49, 000),
                isLoop: false);

            hideMenu5Item.MediaEnded += (o, e) =>
            {
                menuLoopItem.Play();
                CurrentState = TimeLineState.MenuLoop;
            };
            #endregion

            #region 6 пункт меню
            showMenu6Item = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 11, 51, 300),
                timeEnd: new TimeSpan(0, 0, 11, 55, 810),
                isLoop: false);

            showMenu6Item.MediaEnded += (o, e) =>
            {
                menu6LoopItem.Play();
                CurrentState = TimeLineState.MenuItem6Loop;
            };

            menu6LoopItem = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 11, 55, 810),
                timeEnd: new TimeSpan(0, 0, 12, 50, 060),
                isLoop: true,
                isNewStart: false);

            hideMenu6Item = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 12, 50, 070),
                timeEnd: new TimeSpan(0, 0, 12, 55, 700),
                isLoop: false);

            hideMenu6Item.MediaEnded += (o, e) =>
            {
                menuLoopItem.Play();
                CurrentState = TimeLineState.MenuLoop;
            };
            #endregion

            #region Бурение
            showDrillingItem = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 13, 57, 000),
                timeEnd: new TimeSpan(0, 0, 14, 0, 350),
                isLoop: false);

            showDrillingItem.MediaEnded += (o, e) =>
            {
                loopDrillingItem.Play();
                CurrentState = TimeLineState.LoopDrilling;
            };

            loopDrillingItem = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 14, 0, 350),
                timeEnd: new TimeSpan(0, 0, 15, 01, 190),
                isLoop: true,
                isNewStart: false);

            hideDrillingItem = new VideoItem(
                mediaElement: mediaElement,
                timelineTimer: timelineTimer,
                timeBegin: new TimeSpan(0, 0, 15, 01, 200),
                timeEnd: new TimeSpan(0, 0, 15, 06, 470),
                isLoop: false);

            hideDrillingItem.MediaEnded += (o, e) =>
            {
                menuLoopItem.Play();
                CurrentState = TimeLineState.MenuLoop;
            };
            #endregion

            timelineTimer.Interval = TimeSpan.FromMilliseconds(100);
            timelineTimer.Start();
        }

        private TimeLineState currentState = TimeLineState.Idle;
        //private Unosquare.FFME.MediaElement mediaPlayer;

        public TimeLineState CurrentState
        {
            get
            {
                return currentState;
            }

            set
            {
                if (currentState != value)
                {
                    currentState = value;
                    CurrentStateChanged?.Invoke(null, null);
                }
            }
        }

        public void ShowMenu()
        {
            showMenuItem.Play();
            CurrentState = TimeLineState.ShowMenu;
        }

        public void HideMenu()
        {
            menuLoopItem.Stop();
            hideMenuItem.Play();
            CurrentState = TimeLineState.BeginHideMenu;
        }

        public void ShowMenu1()
        {
            menuLoopItem.Stop();
            showMenu1Item.Play();
            CurrentState = TimeLineState.ShowMenuItem1;
        }

        public void HideMenu1()
        {
            menu1LoopItem.Stop();
            hideMenu1Item.Play();
            CurrentState = TimeLineState.HideMenuItem1;
        }

        public void ShowMenu2()
        {
            menuLoopItem.Stop();
            showMenu2Item.Play();
            CurrentState = TimeLineState.ShowMenuItem2;
        }

        public void HideMenu2()
        {
            menu2LoopItem.Stop();
            hideMenu2Item.Play();
            CurrentState = TimeLineState.HideMenuItem2;
        }

        public void ShowMenu3()
        {
            menuLoopItem.Stop();
            showMenu3Item.Play();
            CurrentState = TimeLineState.ShowMenuItem3;
        }

        public void HideMenu3()
        {
            menu3LoopItem.Stop();
            hideMenu3Item.Play();
            CurrentState = TimeLineState.HideMenuItem3;
        }

        public void ShowMenu4()
        {
            menuLoopItem.Stop();
            showMenu4Item.Play();
            CurrentState = TimeLineState.ShowMenuItem4;
        }

        public void HideMenu4()
        {
            menu4LoopItem.Stop();
            hideMenu4Item.Play();
            CurrentState = TimeLineState.HideMenuItem4;
        }

        public void ShowMenu5()
        {
            menuLoopItem.Stop();
            showMenu5Item.Play();
            CurrentState = TimeLineState.ShowMenuItem5;
        }

        public void HideMenu5()
        {
            menu5LoopItem.Stop();
            hideMenu5Item.Play();
            CurrentState = TimeLineState.HideMenuItem5;
        }

        public void ShowMenu6()
        {
            menuLoopItem.Stop();
            showMenu6Item.Play();
            CurrentState = TimeLineState.ShowMenuItem6;
        }

        public void HideMenu6()
        {
            menu6LoopItem.Stop();
            hideMenu6Item.Play();
            CurrentState = TimeLineState.HideMenuItem6;
        }

        public void ShowDrilling()
        {
            menuLoopItem.Stop();
            showDrillingItem.Play();
            CurrentState = TimeLineState.ShowDrilling;
        }

        public void HideDrilling()
        {
            loopDrillingItem.Stop();
            hideDrillingItem.Play();
            CurrentState = TimeLineState.HideDrilling;
        }
    }
}
