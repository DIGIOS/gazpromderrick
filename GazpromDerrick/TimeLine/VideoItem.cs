﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Threading;

namespace GazpromDerrick.TimeLine
{
    public class VideoItem
    {
        private LibVLCSharp.Shared.MediaPlayer mediaElement;
        private TimeSpan timeBegin;
        private TimeSpan timeEnd;
        private bool isLoop = false;
        private bool isNewStart = false;

        public event EventHandler MediaEnded;

        public VideoItem(LibVLCSharp.Shared.MediaPlayer mediaElement, DispatcherTimer timelineTimer, TimeSpan timeBegin, TimeSpan timeEnd, bool isLoop, bool isNewStart = true)
        {
            this.mediaElement = mediaElement;
            this.timeBegin = timeBegin;
            this.timeEnd = timeEnd;
            this.isLoop = isLoop;
            this.isNewStart = isNewStart;

            timelineTimer.Tick += Tick;
        }

        private bool isActive = false;
        public bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public void Play()
        {
            isActive = true;
            if (isNewStart)
            {
                //mediaElement.Position = timeBegin;
                mediaElement.Time = (long)timeBegin.TotalMilliseconds;
                if (!mediaElement.IsPlaying)
                    mediaElement.Play();
            }
        }

        public void Stop()
        {
            isActive = false;
            //mediaElement.Pause();
        }

        private void Tick(object sender, EventArgs e)
        {
            if (!isActive)
                return;

            if (mediaElement.Time >= (long)timeEnd.TotalMilliseconds)
            {
                if (isLoop)
                {
                    //mediaElement.Position = timeBegin;
                    //mediaElement.Seek(timeBegin);
                    //mediaElement.Play();
                    mediaElement.Time = (long)timeBegin.TotalMilliseconds;
                    if (!mediaElement.IsPlaying)
                        mediaElement.Play();
                }
                else
                {
                    isActive = false;
                    MediaEnded?.Invoke(this, new EventArgs());
                }
            }
        }
    }
}
