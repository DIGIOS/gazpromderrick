﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Windows;

namespace GazpromDerrick
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public App()
        {
            this.Dispatcher.UnhandledException += OnDispatcherUnhandledException;

            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(ExceptionHandler);

            LibVLCSharp.Shared.Core.Initialize();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            logger.Info("        =============  Started App v0.2.1  =============        ");
            base.OnStartup(e);
        }

        void OnDispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            logger.Error(e);
            //string errorMessage = string.Format("An unhandled exception occurred: {0}", e.Exception.Message);
            //MessageBox.Show(errorMessage, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            e.Handled = true;
        }

        static void ExceptionHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            logger.Error(e);
        }
    }
}

