﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace GazpromDerrickFrom.Controls
{
    public class DrillingControl
    {
        public event EventHandler ButtonClick;

        private List<Button> buttons = new List<Button>();

        private bool isVisible = true;

        public DrillingControl()
        {

        }

        public bool IsVisible
        {
            get
            {
                return isVisible;
            }

            set
            {
                if (isVisible == value)
                    return;

                isVisible = value;

                foreach (Button button in buttons)
                    button.Visible = isVisible;
            }
        }

        public void InitializeComponent(Form form)
        {
            Point center = new Point(1080 / 2, 1920 / 2);
        }

        private void Button_Click(object sender, EventArgs e)
        {
            ButtonClick?.Invoke(sender, e);
        }
    }
}
