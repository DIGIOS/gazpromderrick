﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace GazpromDerrickFrom.Controls
{
    public class MainMenuControl
    {
        public event EventHandler MenuItemClick;

        public event EventHandler ToLayoutClick;

        public event EventHandler DrillingClick;

        private List<Button> buttons = new List<Button>();

        private bool isVisible = true;

        public MainMenuControl()
        {
        }

        public bool IsVisible
        {
            get
            {
                return isVisible;
            }

            set
            {
                if (isVisible == value)
                    return;

                isVisible = value;

                foreach (Button button in buttons)
                    button.Visible = isVisible;
            }
        }

        public void InitializeComponent(Form form)
        {
            Point center = new Point(1080 / 2, 1920 / 2);

            Button button = GetMenuButton(new Point(center.X + 168, center.Y - 318), "menuItem1");
            form.Controls.Add(button);
            buttons.Add(button);

            button = GetMenuButton(new Point(center.X + 168, center.Y - 192), "menuItem2");
            form.Controls.Add(button);
            buttons.Add(button);

            button = GetMenuButton(new Point(center.X + 168, center.Y - 66), "menuItem3");
            form.Controls.Add(button);
            buttons.Add(button);

            button = GetMenuButton(new Point(center.X + 168, center.Y + 60), "menuItem4");
            form.Controls.Add(button);
            buttons.Add(button);

            button = GetMenuButton(new Point(center.X + 168, center.Y + 186), "menuItem5");
            form.Controls.Add(button);
            buttons.Add(button);

            button = GetMenuButton(new Point(center.X + 168, center.Y + 312), "menuItem6");
            form.Controls.Add(button);
            buttons.Add(button);
        }

        private Button GetMenuButton(Point point, string name)
        {
            Button button = new Button();
            button.Location = point;
            button.Size = new System.Drawing.Size(245, 88);
            button.UseVisualStyleBackColor = true;
            button.TabStop = false;
            button.Name = name;

            button.Click += menuItem_Click;

            return button;
        }

        private void menuItem_Click(object sender, EventArgs e)
        {
            MenuItemClick?.Invoke(sender, e);
        }

        private void toLayout_Click(object sender, EventArgs e)
        {
            ToLayoutClick?.Invoke(sender, e);
        }

        private void DrillingButton_Click(object sender, EventArgs e)
        {
            DrillingClick?.Invoke(sender, e);
        }
    }
}
