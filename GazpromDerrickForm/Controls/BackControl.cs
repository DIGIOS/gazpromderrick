﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GazpromDerrickFrom.Controls
{
    public class BackControl
    {
        public event EventHandler CloseClick;

        private Dictionary<string, Point> position = new Dictionary<string, Point>();

        private bool isVisible = true;

        private Button closeButton;
        private Button backButton;

        public BackControl()
        {
            position.Add("MenuItem1", new Point(25, 960));
            position.Add("MenuItem2", new Point(25, 960));
            position.Add("MenuItem3", new Point(25, 960));
            position.Add("MenuItem4", new Point(25, 960));
            position.Add("MenuItem5", new Point(193, 420));
            position.Add("MenuItem6", new Point(25, 960));
        }

        public bool IsVisible
        {
            get
            {
                return isVisible;
            }

            set
            {
                if (isVisible == value)
                    return;

                isVisible = value;

                backButton.Visible = isVisible;
                closeButton.Visible = isVisible;
            }
        }

        public void SetPosition(string menuItemName)
        {
            Point margin = position[menuItemName];

            if (margin == null)
                margin = new Point(0);

            closeButton.Location = new Point(1080 - closeButton.Width - margin.X, 1920 - closeButton.Height - margin.Y);
        }

        public void InitializeComponent(Form from)
        {
            backButton = new Button();
            backButton.Location = new Point(40, 1777);
            backButton.Size = new System.Drawing.Size(245, 88);
            backButton.UseVisualStyleBackColor = true;
            backButton.TabStop = false;
            backButton.Name = "backButton";

            backButton.Click += backButton_Click;

            closeButton = new Button();
            closeButton.Location = new Point(0, 0);
            closeButton.Size = new System.Drawing.Size(50, 50);
            closeButton.UseVisualStyleBackColor = true;
            closeButton.TabStop = false;
            closeButton.Name = "closeButton";

            closeButton.Click += backButton_Click;

            from.Controls.Add(backButton);
            from.Controls.Add(closeButton);
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            CloseClick?.Invoke(sender, e);
        }
    }
}
