﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GazpromDerrickFrom
{
    public partial class BottomForm : Form
    {
        private string currentDirectory = "";

        public BottomForm()
        {
            InitializeComponent();

            var currentAssembly = Assembly.GetEntryAssembly();
            currentDirectory = new FileInfo(currentAssembly.Location).DirectoryName;

            vlcControl.Play(new Uri(Path.Combine(currentDirectory, "Data", "niz_s-well.wmv")));
        }

        private void Form2_Shown(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

            List<Screen> screenList = Screen.AllScreens.ToList<Screen>();

            Screen leftScreen = screenList.Find(x => x.DeviceName.EndsWith(Properties.Settings.Default.DisplayLeft));

            this.Left = leftScreen.Bounds.Left;
            this.Top = leftScreen.Bounds.Top;
            this.Width = leftScreen.Bounds.Width * 3;
            this.Height = leftScreen.Bounds.Height;
        }

        private void vlcControl_VlcLibDirectoryNeeded(object sender, Vlc.DotNet.Forms.VlcLibDirectoryNeededEventArgs e)
        {
            e.VlcLibDirectory = new DirectoryInfo(Path.Combine(currentDirectory, "libvlc", IntPtr.Size == 4 ? "win-x86" : "win-x64"));
        }
    }
}
