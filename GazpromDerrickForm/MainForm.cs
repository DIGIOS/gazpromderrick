﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GazpromDerrickFrom.Controls;
using GazpromDerrickFrom.TimeLine;
using VideoTest2.Controls;

namespace GazpromDerrickFrom
{
    public partial class MainForm : Form
    {
        private BottomForm bottomForm;

        private string currentDirectory;

        private ActionTimeline timeLine;

        private MainMenuControl mainMenu;
        private DrillingControl drillingControl;
        private BackControl backControl;

        public MainForm()
        {
            mainMenu = new MainMenuControl();
            mainMenu.InitializeComponent(this);
            mainMenu.MenuItemClick += mainMenu_MenuItemClick;

            backControl = new BackControl();
            backControl.InitializeComponent(this);
            backControl.CloseClick += backControl_CloseClick;

            drillingControl = new DrillingControl();

            GButton gbutton = new GButton();
            gbutton.Size = new Size(245, 88);
            gbutton.Location = new Point(522, 174);
            Controls.Add(gbutton);

            InitializeComponent();

            bottomForm = new BottomForm();

            vlcControl.SetMedia(new Uri(Path.Combine(GetCurrentDirectory(), "Data", "verh_full.mp4")));
            //vlcControl.Play();
            //vlcControl.Pause();

            timeLine = new ActionTimeline(vlcControl);
            timeLine.CurrentStateChanged += TimelineStateChanged;

            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GoFullscreen(true);
            timeLine.ShowMenu();

            bottomForm.Show();

            /*this.WindowState = FormWindowState.Normal;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

            this.Bounds = Screen.AllScreens[0].Bounds;*/

            //vlcControl.Play(new Uri(Path.Combine(GetCurrentDirectory(), "Data", "verh_full.wmv")));

            //timeLine.ShowMenu2();
        }

        private void GoFullscreen(bool fullscreen)
        {
            if (fullscreen)
            {
                this.WindowState = FormWindowState.Normal;
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                this.Bounds = Screen.PrimaryScreen.Bounds;
            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            }
        }

        private string GetCurrentDirectory()
        {
            if (string.IsNullOrEmpty(currentDirectory))
            {
                var currentAssembly = Assembly.GetEntryAssembly();
                currentDirectory = new FileInfo(currentAssembly.Location).DirectoryName;
            }

            return currentDirectory;
        }

        private void vlcControl_VlcLibDirectoryNeeded(object sender, Vlc.DotNet.Forms.VlcLibDirectoryNeededEventArgs e)
        {
            e.VlcLibDirectory = new DirectoryInfo(Path.Combine(GetCurrentDirectory(), "libvlc", IntPtr.Size == 4 ? "win-x86" : "win-x64"));
            vlcControl.VlcMediaplayerOptions = new string[]
            {
                "--file-caching=300", "--input-fast-seek",
                "--file-logging", "-vvv", "--extraintf=logger", "--logfile=Logs.log"
            };
        }

        private void vlcControl_PositionChanged(object sender, Vlc.DotNet.Core.VlcMediaPlayerPositionChangedEventArgs e)
        {
            //Console.WriteLine("pos: " + e.NewPosition);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timeLine.ShowMenu3();
        }

        private void vlcControl_TimeChanged(object sender, Vlc.DotNet.Core.VlcMediaPlayerTimeChangedEventArgs e)
        {
            //Console.WriteLine("time: " + e.NewTime);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timeLine.HideMenu3();
        }

        private void mainMenu_MenuItemClick(object sender, EventArgs e)
        {
            Button bt = (sender as Button);
            if (bt.Name == "menuItem1")
            {
                timeLine.ShowMenu1();
            }
            else if (bt.Name == "menuItem2")
            {
                timeLine.ShowMenu2();
            }
            else if (bt.Name == "menuItem3")
            {
                timeLine.ShowMenu3();
            }
            else if (bt.Name == "menuItem4")
            {
                timeLine.ShowMenu4();
            }
            else if (bt.Name == "menuItem5")
            {
                timeLine.ShowMenu5();
            }
            else if (bt.Name == "menuItem6")
            {
                timeLine.ShowMenu6();
            }
        }

        private void TimelineStateChanged(object sender, EventArgs e)
        {
            mainMenu.IsVisible = false;
            backControl.IsVisible = false;
            drillingControl.IsVisible = false;

            switch (timeLine.CurrentState)
            {
                case ActionTimeline.TimeLineState.MenuLoop:
                    mainMenu.IsVisible = true;
                    break;
                case ActionTimeline.TimeLineState.MenuItem1Loop:
                    backControl.SetPosition("MenuItem1");
                    backControl.IsVisible = true;
                    break;
                case ActionTimeline.TimeLineState.MenuItem2Loop:
                    backControl.SetPosition("MenuItem2");
                    backControl.IsVisible = true;
                    break;
                case ActionTimeline.TimeLineState.MenuItem3Loop:
                    backControl.SetPosition("MenuItem3");
                    backControl.IsVisible = true;
                    break;
                case ActionTimeline.TimeLineState.MenuItem4Loop:
                    backControl.SetPosition("MenuItem4");
                    backControl.IsVisible = true;
                    break;
                case ActionTimeline.TimeLineState.MenuItem5Loop:
                    backControl.SetPosition("MenuItem5");
                    backControl.IsVisible = true;
                    break;
                case ActionTimeline.TimeLineState.MenuItem6Loop:
                    backControl.SetPosition("MenuItem6");
                    backControl.IsVisible = true;
                    break;
                case ActionTimeline.TimeLineState.LoopDrilling:
                    drillingControl.IsVisible = true;
                    break;
                case ActionTimeline.TimeLineState.HideMenu:
                    //layoutBlank.BeginFadeIn();
                    break;
                default:
                    break;
            }
        }

        private void backControl_CloseClick(object sender, EventArgs e)
        {
            switch (timeLine.CurrentState)
            {
                case ActionTimeline.TimeLineState.MenuItem1Loop:
                    timeLine.HideMenu1();
                    break;
                case ActionTimeline.TimeLineState.MenuItem2Loop:
                    timeLine.HideMenu2();
                    break;
                case ActionTimeline.TimeLineState.MenuItem3Loop:
                    timeLine.HideMenu3();
                    break;
                case ActionTimeline.TimeLineState.MenuItem4Loop:
                    timeLine.HideMenu4();
                    break;
                case ActionTimeline.TimeLineState.MenuItem5Loop:
                    timeLine.HideMenu5();
                    break;
                case ActionTimeline.TimeLineState.MenuItem6Loop:
                    timeLine.HideMenu6();
                    break;
                default:
                    break;
            }
        }
    }
}
