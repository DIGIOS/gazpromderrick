﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace GazpromDerrickFrom.TimeLine
{
    public class VideoItem
    {
        private Vlc.DotNet.Forms.VlcControl mediaElement;
        private TimeSpan timeBegin;
        private TimeSpan timeEnd;
        private bool isLoop = false;
        private bool isNewStart = false;

        public event EventHandler MediaEnded;

        public VideoItem(Vlc.DotNet.Forms.VlcControl mediaElement, DispatcherTimer timelineTimer, TimeSpan timeBegin, TimeSpan timeEnd, bool isLoop, bool isNewStart = true)
        {
            this.mediaElement = mediaElement;
            this.timeBegin = timeBegin;
            this.timeEnd = timeEnd;
            this.isLoop = isLoop;
            this.isNewStart = isNewStart;

            timelineTimer.Tick += Tick;
        }

        private bool isActive = false;
        public bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public void Play()
        {
            isActive = true;
            if (isNewStart)
            {
                mediaElement.Time = (long)timeBegin.TotalMilliseconds;
                //mediaElement.Position = (float)timeBegin.TotalMilliseconds / 1000000f;
                mediaElement.Play();
            }
        }

        public void Stop()
        {
            isActive = false;
            //mediaElement.Pause();
        }

        private void Tick(object sender, EventArgs e)
        {
            if (!isActive)
                return;

            if (mediaElement.Time >= (long)timeEnd.TotalMilliseconds)
            //if (mediaElement.Position >= (float)timeEnd.TotalMilliseconds / 1000000f)
            {
                if (isLoop)
                {
                    mediaElement.Time = (long)timeBegin.TotalMilliseconds;
                    //mediaElement.Position = (float)timeBegin.TotalMilliseconds / 1000000f;
                    mediaElement.Play();
                }
                else
                {
                    isActive = false;
                    MediaEnded?.Invoke(this, new EventArgs());
                }
            }
        }
    }
}
